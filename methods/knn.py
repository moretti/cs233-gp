import numpy as np

class KNN(object):
    """
        kNN classifier object.
        Feel free to add more functions to this class if you need.
        But make sure that __init__, set_arguments, fit and predict work correctly.
    """

    def __init__(self, *args, **kwargs):
        """
            Initialize the task_kind (see dummy_methods.py)
            and call set_arguments function of this class.
        """

        self.task_kind = 'classification'
        self.set_arguments(*args, **kwargs)

    def set_arguments(self, *args, **kwargs):
        """
            args and kwargs are super easy to use! See dummy_methods.py
            The KNN class should have a variable defining the number of neighbours (k).
            You can either pass this as an arg or a kwarg.
        """
        
        if "k" in kwargs:
            self.k = kwargs["k"]
        elif len(args) > 0:
            self.k = args[0]
        else:
            self.k = 1
   

    def euclidian_dist(self, example, training_examples):
        return np.sqrt(np.sum(np.square(training_examples - example), axis = 1))

    def find_k_nearest_neighbors(self, k, distances):
        indices = (np.argsort(distances))
        ind = indices[: self.k]
        return ind
    
    def predict_label(self, neighbor_labels):
        return np.argmax(np.bincount(neighbor_labels))

    def choose_random_sample(self, data):
        return data[np.random.randint(0, high = data.shape[0]+1),:]

    def kNN_one_example(self, unlabeled_example, training_features, training_labels, k):
        distances = self.euclidian_dist(unlabeled_example, training_features) 
        nn_indices = self.find_k_nearest_neighbors(k, distances) 
        neighbor_labels = training_labels[nn_indices]
        best_label = self.predict_label(neighbor_labels)
        return best_label

    def kNN(self, unlabeled, training_features, training_labels, k):
        return np.apply_along_axis(func1d=self.kNN_one_example, axis=1, arr=unlabeled, 
                               training_features=training_features, 
                               training_labels=training_labels, k=self.k)
    
    def fit(self, training_data, training_labels):
        self.training_data = training_data
        self.training_labels = training_labels
        return training_labels
                               
    def predict(self, test_data):
        return self.kNN(test_data, self.training_data, self.training_labels, self.k)