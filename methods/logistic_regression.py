import numpy as np
import sys
sys.path.append('..')
from utils import label_to_onehot, onehot_to_label

class LogisticRegression(object):
    """
        LogisticRegression classifier object.
        Feel free to add more functions to this class if you need.
        But make sure that __init__, set_arguments, fit and predict work correctly.
    """
    """
        I assume for the sake of my mental health that this is for a binary classification task. 
        I'll change it if it must be changed, but you can't do regression with a logistic regression object.
        unfortunately. 
    """

    def __init__(self, *args, **kwargs):
        """
            Initialize the task_kind (see dummy_methods.py)
            and call set_arguments function of this class.
        """
        
        self.task_kind = 'classification'
        self.set_arguments(*args, **kwargs)

    def set_arguments(self, *args, **kwargs):
        """
            args and kwargs are super easy to use! See dummy_methods.py
            The LogisticRegression class should have variables defining the learning rate (lr)
            and the number of max iterations (max_iters)
            You can either pass these as args or kwargs.
        """
        
        # learning rate
        if "lr" in kwargs: 
            self.lr = kwargs["lr"]
        elif len(args) > 1: 
            self.lr = args[0]
        else: 
            self.lr = 5e-5
        
        # max_iterations
        if "max_iter" in kwargs: 
            self.max_iters = kwargs["max_iter"]
        elif len(args) > 1: 
            self.max_iters = args[1]
        else: 
            self.max_iters = 100

    def f_softmax(self, d, w): 
        """ softmax function.
        Args: 
            d: input array, shape (N, D)
            w: weights, shape (D, C)
        Returns: 
            res: probabilities, shape (N, C) // not sure of this, might need to change
        """
        den = np.sum(np.exp(d @ w), axis=1, keepdims=True)
        num = np.exp(d @ w)

        return num / den


    def loss(self, data, labels, w):
        """ loss for the logistic regression.
        Args: 
            data: dataset, shape (N, D)
            labels: labels, shape (N, C) (one-hot)
            w: weights of the model, shape (D, C)
        Returns: 
            loss: loss of current model (float)
        """

        return -np.sum(labels.T @ np.log(self.f_softmax(data, w)))

    def gradient(self, data, labels, w): 
        """ gradient
        Args:
            data: dataset, shape (N, D)
            labels: labels, shape (N, C)
            w: current weights of the model, shape (D, C)
        Returns: 
            grad: gradient of the given function
        """
        return data.T @ (self.f_softmax(data, w) - labels)

    def classify(self, data, w):
        """ solves the classification task. 
        Args: 
            data: dataset, shape (N, D)
            w: weights, shape (D, C)
        Returns: 
            pred: the predicted labels for every element, shape (N, C)    
        """ 
        
        return data @ w

    def accuracy(self, l_ground_truth, l_prediction): 
        """ computes the accuracy of a model given the ground truth and the prediction
        Args: 
            l_ground_truth: verified labels for the test we ran, shape (N, C)
            l_prediction: computed labels by the classification model, shape (N, C)
        Returns: 
            acc: the accuracy of the model (float)
        """

        collapsed_gt = onehot_to_label(l_ground_truth) if len(l_ground_truth.shape) > 1 else l_ground_truth
        collapsed_pr = onehot_to_label(l_prediction) if len(l_prediction.shape) > 1 else l_prediction

        return l_ground_truth[collapsed_gt == collapsed_pr].size / collapsed_gt.size

    def fit(self, training_data, training_labels):
        """
            Trains the model, returns predicted labels for training data.
            Arguments:
                training_data (np.array): training data of shape (N, D)
                training_labels (np.array): regression target of shape (N, )
            Returns:
                pred_labels (np.array): target of shape (N, )
        """

        self.input_correct = len(training_labels.shape) > 1

        expanded_labels = training_labels if self.input_correct else label_to_onehot(training_labels)

        self.k = expanded_labels.shape[1] # number of classes we're working on

        w = np.random.normal(0, 0.1, [training_data.shape[1], self.k]) # random weight init! moche

        for i in range(self.max_iters): 
            
            grad = self.gradient(training_data, expanded_labels, w)
            w = w - self.lr * grad # iteration step

            pred = self.classify(training_data, w)
            ## what about cross validation?
            acc = self.accuracy(pred, training_labels) 
            if acc == 1: 
               break # done with this! we're perfect!

        self.w = w

        return pred if self.input_correct else np.argmax(pred, axis=1)

    def predict(self, test_data):
        """
            Runs prediction on the test data.
            
            Arguments:
                test_data (np.array): test data of shape (N,D)
            Returns:
                test_
                labels (np.array): labels of shape (N, )
        """

        pred = self.classify(test_data, self.w)

        return pred if self.input_correct else np.argmax(pred, axis=1)